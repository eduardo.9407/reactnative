import React, { useState, useEffect, Fragment } from 'react'
import { StatusBar } from 'react-native'
import { AppLoading } from 'expo'
import * as Font from 'expo-font'
import Routes from './screens/Routes'

const App = () => {

  const [fontLoaded, setFontLoaded] = useState(false)

  async function loadingFont() {
    await Font.loadAsync({
      'dogbyte': require('./assets/fonts/dogbyte.otf')
    })
    setFontLoaded(true)
  }

  useEffect(() => {
    loadingFont()
  })

  if (fontLoaded) {
    return (
      <Fragment>
        <StatusBar barStyle="light-content"/>
        <Routes />
      </Fragment>
    )
  }

  return (<AppLoading/>)

}

export default App
