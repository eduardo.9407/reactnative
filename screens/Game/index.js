import React,  { Fragment, useState, useEffect } from 'react'
import { View, Dimensions, TouchableOpacity, Text, Image } from 'react-native'
import { Header } from '../../components'
import { generateRGB, mutateRGB } from '../../utilities'
import styles from './style'

const Game = () => {

  const [points, setPoints] = useState(0)
  const [timeLeft, setTimeLeft] = useState(15)
  const [rgb, setRGB] = useState(generateRGB())
  const [size, setSize] = useState(0)
  const [tileIndex, setTileIndex] = useState(0,0)
  const [tileColor, setTileColor] = useState()
  const [gameState, setGameState] = useState('INGAME')

  generateSizeIndex = size => {
    return Math.floor(Math.random() * size)
  }

  generateNewRound = () => {
    const RGB = generateRGB()
    const mRGB = mutateRGB(RGB)
    const nSize = Math.min(Math.max(Math.floor(Math.sqrt(points)), 2), 5)

    setSize(nSize)
    setTileIndex([generateSizeIndex(nSize), generateSizeIndex(nSize)])
    setTileColor(`rgb(${mRGB.r}, ${mRGB.g}, ${mRGB.b})`)
    setRGB(RGB)
  }

  onTilePress = (rowIndex, columnIndex) => {

    if (rowIndex == tileIndex[0] && columnIndex == tileIndex[1]) {
      setPoints(points + 1)
      setTimeLeft(timeLeft + 2)
    } else {
      setTimeLeft(timeLeft - 2)
    }

    generateNewRound()
  }

  onExitPress = () => {
    console.log('Exit.')
  }

  useEffect(() => {
    generateNewRound()
  }, [])

  useEffect(() => {

    const intervalTimerID = setInterval(() => {
      if (gameState === 'INGAME') {
        if (timeLeft <= 0) {
          setGameState('LOST')
        } else {
          setTimeLeft(timeLeft - 1)
        }
      }

    }, 1000)

    return () => clearInterval(intervalTimerID)
  }, [gameState, timeLeft])

  onBottomBarPress = async () => {
    switch(gameState) {
      case 'INGAME':
        setGameState('PAUSED')
        break
      case 'PAUSED':
        setGameState('INGAME')
        break
      case 'LOST':
        await setPoints(0)
        await setTimeLeft(15)
        await setGameState('INGAME')
        generateNewRound()
        break
    }
  }

  const { width } = Dimensions.get('window')

  const bottomIcon =
    gameState === 'INGAME'
      ? require('../../assets/icons/pause.png')
    : gameState === 'PAUSED'
      ? require('../../assets/icons/play.png')
    : require('../../assets/icons/replay.png')

  return (
    <View style={styles.container}>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Header />
      </View>

      <View
        style={{
          height: width * 0.875,
          width: width * 0.875,
          flexDirection: 'row'
        }}
      >

        {
          gameState === 'INGAME' ?
              /* COLUMNS */
            Array(size).fill().map((val, columnIndex) => (
              <View style={{ flex: 1, flexDirection: 'column '}} key={columnIndex}>
                { /* TILES */ }
                {Array(size).fill().map((val, rowIndex) => (
                  <TouchableOpacity
                    key={`${rowIndex}.${columnIndex}}`}
                    style={{
                        flex: 1,
                        backgroundColor: rowIndex == tileIndex[0] && columnIndex == tileIndex[1]
                          ? tileColor
                          : `rgb(${rgb.r}, ${rgb.g}, ${rgb.b})`,
                        margin: 2
                    }}
                    onPress={() => onTilePress(rowIndex, columnIndex)}
                  />
                ))}
              </View>
            ))
          : (
            <View style={styles.pausedContainer}>
              {gameState === "PAUSED" ? (
                <Fragment>
                  <Image
                    source={require("../../assets/icons/mug.png")}
                    style={styles.pausedIcon}
                  />
                  <Text style={styles.pausedText}>...</Text>
                </Fragment>
              ) : (
                <Fragment>
                  <Image
                    source={require("../../assets/icons/dead.png")}
                    style={styles.pausedIcon}
                  />
                  <Text style={styles.pausedText}>MUERTO</Text>
                </Fragment>
              )}
              <TouchableOpacity onPress={onExitPress}>
                <Image
                  source={require("../../assets/icons/escape.png")}
                  style={styles.exitIcon}
                />
              </TouchableOpacity>
            </View>
          )
        }
      </View>
      <View style={styles.bottomContainer}>
        <View style={styles.bottomSectionContainer}>
          <Text style={styles.counterCount}>
            {points}
          </Text>
          <Text style={styles.counterLabel}>
            points
          </Text>
          <View style={styles.bestContainer}>
            <Image
              source={require('../../assets/icons/trophy.png')}
              style={styles.bestIcon}
            />
            <Text style={styles.bestLabel}>
              0
            </Text>
          </View>


        </View>
        <View style={styles.bottomSectionContainer}>
          <TouchableOpacity
            style={{ alignItems: 'center' }}
            onPress={onBottomBarPress}
          >
            <Image source={bottomIcon} style={styles.pausedIcon}/>
          </TouchableOpacity>
        </View>
        <View style={styles.bottomSectionContainer}>
          <Text style={styles.counterCount}>{timeLeft}</Text>
          <Text style={styles.counterLabel}>seconds left</Text>
        </View>
      </View>
    </View>
  )
}

export default Game
