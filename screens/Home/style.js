import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#08542f",
    justifyContent: 'center',
    alignItems: 'center'
  },
  play: {
    fontSize: 45,
    fontFamily: "dogbyte",
    color: "#ecf0f1",
    marginTop: 5
  },
  playIcon: {
    height: 60,
    width: 60,
    marginRight: 15
  },
  hiScore: {
    fontSize: 28.5,
    fontFamily: 'dogbyte',
    color: '#ecf0f1',
    marginTop: 5
  },
  trophyIcon: {
    height: 45,
    width: 45,
    marginRight: 12.5
  },
  bottomContainer: {
    flexDirection: 'row',
    marginHorizontal: '4%',
    marginBottom: 13
  },
  copyrightText: {
    fontSize: 16,
    fontFamily: 'dogbyte',
    marginBottom: 2.5
  },
  soundIcon: {
    height: 45,
    width: 45
  },
})
