import React, { useState } from 'react'
import styles from './style'
import { Text, View, Image, TouchableOpacity } from 'react-native'

import { Header } from '../../components'

/*
  <View /> => Like divs in HTML
*/

const Home = (props) => {

  const [soundOn, setSoundOn] = useState(true)

  onPlayPress = () => {
    props.navigation.navigate('Game')
  }

  onLeaderboardPress = () => {
    console.log('onLeaderboardPress event handler')
  }

  onToggleSound = () => {
    console.log('onToggleSound event handler')
  }

  const imgSource = soundOn
    ? require('../../assets/icons/speaker-on.png')
    : require('../../assets/icons/speaker-off.png')

  return (
    <View style={styles.container}>
      <View style={{ flex: 1 }}/>
      <Header fontSize={50} />
      <TouchableOpacity
        onPress={onPlayPress}
        style={{
          marginTop: 80,
          flexDirection: 'row',
          alignItems: 'center'
        }}
      >
        <Image
          source={require('../../assets/icons/play_arrow.png')}
          style={styles.playIcon}
        />
        <Text style={styles.play}>PLAY!</Text>
      </TouchableOpacity>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginTop: 20
        }}
      >
        <Image
          source={require('../../assets/icons/trophy.png')}
          style={styles.trophyIcon}
        />

        <Text style={styles.hiScore}>
          Hi-Score: 0
        </Text>
      </View>
      <TouchableOpacity onPress={onLeaderboardPress}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginTop: 80
        }}
      >
        <Image
          source={require('../../assets/icons/leaderboard.png')}
          style={styles.trophyIcon}
        />
        <Text style={styles.hiScore}>Leaderboard</Text>
      </TouchableOpacity>
      <View style={{ flex: 1 }}/>

      <View style={styles.bottomContainer}>
        <View>
          <Text style={[styles.copyrightText, { color: "#E64C3C" }]}>
            Music: Komiku
          </Text>
          <Text style={[styles.copyrightText, { color: "#F1C431" }]}>
            SFX: SubspaceAudio
          </Text>
          <Text style={[styles.copyrightText, { color: "#3998DB" }]}>
            Dev: RisingStack
          </Text>
        </View>
        <View style={{ flex: 1 }}/>
        <TouchableOpacity onPress={onToggleSound}>
          <Image source={imgSource} style={styles.soundIcon}/>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default Home
