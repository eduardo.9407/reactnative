import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation'
import Home from './Home'
import Game from './Game'

/*
createStackNavigator.-
It initializes a new stack navigator that
returns a React component that we can render.
*/

const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: Home
    },
    Game: {
      screen: Game,
      navigationOptions: {
        gesturesEnabled: false,
      }
    }
  },
  {
    initialRouteName: "Home",
    headerMode: "none",
  }
)

export default createAppContainer(AppNavigator)
